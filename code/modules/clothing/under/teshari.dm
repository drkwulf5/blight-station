/obj/item/clothing/under/teshari
	name = "teshari base uniform"
	desc = "you shouldn't see this in normal gameplay."
	item_icons = list(slot_uniform_str = 'icons/mob/species/teshari/onmob_under_teshari.dmi')
	icon = 'icons/obj/clothing/species/teshari/obj_under_teshari.dmi'
	icon_state = "teshari_grey"
	worn_state = "teshari_grey"
	species_restricted = list(SPECIES_TESHARI)

/obj/item/clothing/under/teshari/smock
	name = "small grey smock"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_grey"
	worn_state = "teshari_grey"

/obj/item/clothing/under/teshari/smock/red
	name = "small red smock"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_red"
	worn_state = "teshari_red"

/obj/item/clothing/under/teshari/smock/white
	name = "small smock"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white"
	worn_state = "teshari_white"

/obj/item/clothing/under/teshari/smock/yellow
	name = "small yellow smock"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_yellow"
	worn_state = "teshari_yellow"

/obj/item/clothing/under/teshari/smock/rainbow
	name = "small rainbow smock"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_rainbow"
	worn_state = "teshari_rainbow"

/obj/item/clothing/under/teshari/uniform
	name = "small uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_greydress"
	worn_state = "teshari_greydress"

/obj/item/clothing/under/teshari/uniform/bluegrey
	name = "small blue and grey uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_bluegreydress"
	worn_state = "teshari_bluegreydress"

/obj/item/clothing/under/teshari/uniform/black
	name = "small black utility uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_blackutility"
	worn_state = "teshari_blackutility"

/obj/item/clothing/under/teshari/uniform/med
	name = "small medical uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_medical"
	worn_state = "teshari_medical"

/obj/item/clothing/under/teshari/uniform/med/alt
	name = "small medical dress"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "tesh_dress_medical"
	worn_state = "tesh_dress_medical"

/obj/item/clothing/under/teshari/uniform/sci
	name = "small research uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_science"
	worn_state = "teshari_science"

/obj/item/clothing/under/teshari/uniform/sci/alt
	name = "small research dress"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_dress_science"
	worn_state = "teshari_dress_science"

/obj/item/clothing/under/teshari/uniform/sec
	name = "small security uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "tesh_smock_security"
	worn_state = "tesh_smock_security"

/obj/item/clothing/under/teshari/uniform/sec/alt
	name = "small security dress"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "tesh_dress_security"
	worn_state = "tesh_dress_security"

/obj/item/clothing/under/teshari/uniform/engi
	name = "small engineering uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "tesh_smock_engine"
	worn_state = "tesh_smock_engine"

/obj/item/clothing/under/teshari/uniform/engi/alt
	name = "small engineering dress"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "tesh_dress_engine"
	worn_state = "tesh_dress_engine"

/obj/item/clothing/under/teshari/uniform/cap
	name = "small command uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_captain"
	worn_state = "teshari_captain"

/obj/item/clothing/under/teshari/uniform/cap/alt
	name = "small command dress"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_dress_cap"
	worn_state = "teshari_dress_cap"

/obj/item/clothing/under/teshari/uniform/cap/alt2
	name = "small formal command uniform"
	desc = "A small uniform fitted for a Teshari."
	icon_state = "teshari_captain_formal"
	worn_state = "teshari_captain_formal"

/obj/item/clothing/under/teshari/worksuit
	name = "small black and red worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_black_red_worksuit"
	worn_state = "teshari_black_red_worksuit"

/obj/item/clothing/under/teshari/worksuit/blackpurple
	name = "small black and purple worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_black_purple_worksuit"
	worn_state = "teshari_black_purple_worksuit"

/obj/item/clothing/under/teshari/worksuit/blackorange
	name = "small black and orange worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_black_orange_worksuit"
	worn_state = "teshari_black_orange_worksuit"

/obj/item/clothing/under/teshari/worksuit/blackblue
	name = "small black and blue worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_black_blue_worksuit"
	worn_state = "teshari_black_blue_worksuit"

/obj/item/clothing/under/teshari/worksuit/blackgreen
	name = "small black and green worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_black_green_worksuit"
	worn_state = "teshari_black_green_worksuit"

/obj/item/clothing/under/teshari/worksuit/whitered
	name = "small white and red worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white_red_worksuit"
	worn_state = "teshari_white_red_worksuit"

/obj/item/clothing/under/teshari/worksuit/whitepurple
	name = "small white and purple worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white_purple_worksuit"
	worn_state = "teshari_white_purple_worksuit"

/obj/item/clothing/under/teshari/worksuit/whiteorange
	name = "small white and orange worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white_orange_worksuit"
	worn_state = "teshari_white_orange_worksuit"

/obj/item/clothing/under/teshari/worksuit/whiteblue
	name = "small white and blue worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white_blue_worksuit"
	worn_state = "teshari_white_blue_worksuit"

/obj/item/clothing/under/teshari/worksuit/whitegreen
	name = "small white and green worksuit"
	desc = "It looks fitted to nonhuman proportions."
	icon_state = "teshari_white_green_worksuit"
	worn_state = "teshari_white_green_worksuit"