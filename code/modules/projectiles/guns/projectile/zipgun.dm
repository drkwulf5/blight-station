
/obj/item/weapon/gun/projectile/pirate
	name = "pipe gun"
	desc = "Little more than a barrel, handle, and firing mechanism, cheap makeshift firearms like this one are not uncommon in frontier systems."
	icon = 'icons/obj/guns/zipgun.dmi'
	icon_state = "pipegun"
	item_state = "sawnshotgun"
	handle_casings = CYCLE_CASINGS //player has to take the old casing out manually before reloading
	load_method = SINGLE_CASING
	max_shells = 1 //literally just a barrel
	has_safety = FALSE
	w_class = ITEM_SIZE_NORMAL

	var/global/list/ammo_types = list(
		/obj/item/ammo_casing/pistol,
		/obj/item/ammo_casing/shotgun,
		/obj/item/ammo_casing/shotgun,
		/obj/item/ammo_casing/shotgun/pellet,
		/obj/item/ammo_casing/shotgun/pellet,
		/obj/item/ammo_casing/shotgun/pellet,
		/obj/item/ammo_casing/shotgun/beanbag ,
		/obj/item/ammo_casing/shotgun/stunshell,
		/obj/item/ammo_casing/shotgun/flash,
		/obj/item/ammo_casing/rifle/military,
		/obj/item/ammo_casing/rifle
		)

/obj/item/weapon/gun/projectile/pirate/toggle_safety(var/mob/user)
	to_chat(user, "<span class='warning'>There's no safety on \the [src]!</span>")

/obj/item/weapon/gun/projectile/pirate/Initialize()
	var/obj/item/ammo_casing/ammo = pick(ammo_types)
	caliber = initial(ammo.caliber)
	desc += " Uses [caliber] rounds."
	. = ..()

/obj/item/weapon/gun/projectile/pirate/unloaded
	starts_loaded = FALSE

//Hacky and bad code stolen from the sawn off
/obj/item/weapon/gun/projectile/pirate/attackby(var/obj/item/A as obj, mob/user as mob)
	if(w_class > 3 && (istype(A, /obj/item/weapon/circular_saw) || istype(A, /obj/item/weapon/melee/energy) || istype(A, /obj/item/weapon/gun/energy/plasmacutter)))
		if(istype(A, /obj/item/weapon/gun/energy/plasmacutter))
			var/obj/item/weapon/gun/energy/plasmacutter/cutter = A
			if(!cutter.slice(user))
				return ..()
		to_chat(user, "<span class='notice'>You begin to shorten the barrel of \the [src].</span>")
		if(loaded.len)
			for(var/i in 1 to max_shells)
				Fire(user, user)	//will this work? //it will. we call it twice, for twice the FUN
			user.visible_message("<span class='danger'>The pipe gun goes off!</span>", "<span class='danger'>The pipe gun off in your face!</span>")
			return
		if(do_after(user, 30, src))	//SHIT IS STEALTHY EYYYYY
			user.unEquip(src)
			var/obj/item/weapon/gun/projectile/pirate/pistol/empty/buddy = new(loc)
			transfer_fingerprints_to(buddy)
			qdel(src)
			to_chat(user, "<span class='warning'>You shorten the barrel of \the [src]!</span>")
	else

/obj/item/weapon/gun/projectile/pirate/pistol
	name = "zip gun"
	desc = "What happens when a pipe gun is cut down to a concealable size. These makeshit concealable weapons are used only by the incredibly desperate."
	icon = 'icons/obj/guns/zipgun.dmi'
	icon_state = "zipgun"
	item_state = "pen"
	w_class = ITEM_SIZE_SMALL
	slot_flags = SLOT_POCKET|SLOT_HOLSTER
	accuracy = -2
	accuracy_power = 3 //Point Blank Range Only

/obj/item/weapon/gun/projectile/pirate/pistol/empty
	starts_loaded = FALSE