//'hair'
/datum/sprite_accessory/hair/teshari
	name = "Teshari Plumage"
	icon_state = "teshari_default"
	icon = 'icons/mob/human_races/species/teshari/hair.dmi'
	species_allowed = list(SPECIES_TESHARI)

/datum/sprite_accessory/hair/teshari/ears
	name = "Teshari Ears"
	icon_state = "teshari_ears"
/datum/sprite_accessory/hair/teshari/excited
	name = "Teshari Spiky"
	icon_state = "teshari_spiky"
/datum/sprite_accessory/hair/teshari/hedgehog
	name = "Teshari Hedgehog"
	icon_state = "teshari_hedge"
/datum/sprite_accessory/hair/teshari/long
	name = "Teshari Unpruned"
	icon_state = "teshari_long"
/datum/sprite_accessory/hair/teshari/sunburst
	name = "Teshari Sunburst"
	icon_state = "teshari_burst_short"
/datum/sprite_accessory/hair/teshari/mohawk
	name = "Teshari Mohawk"
	icon_state = "teshari_mohawk"
/datum/sprite_accessory/hair/teshari/pointy
	name = "Teshari Pointy"
	icon_state = "teshari_pointy"
/datum/sprite_accessory/hair/teshari/upright
	name = "Teshari Upright"
	icon_state = "teshari_upright"
/datum/sprite_accessory/hair/teshari/mane
	name = "Teshari Mane"
	icon_state = "teshari_mane"
/datum/sprite_accessory/hair/teshari/droopy
	name = "Teshari Droopy"
	icon_state = "teshari_droopy"
/datum/sprite_accessory/hair/teshari/mushroom
	name = "Teshari Mushroom"
	icon_state = "teshari_mushroom"
/datum/sprite_accessory/hair/teshari/twies
	name = "Teshari Twies"
	icon_state = "teshari_twies"
/datum/sprite_accessory/hair/teshari/backstrafe
	name = "Teshari Backstrafe"
	icon_state = "teshari_backstrafe"
/datum/sprite_accessory/hair/teshari/longway
	name = "Teshari Long way"
	icon_state = "teshari_longway"
/datum/sprite_accessory/hair/teshari/notree
	name = "Teshari Tree"
	icon_state = "teshari_notree"
/datum/sprite_accessory/hair/teshari/fluffymohawk
	name = "Teshari Fluffy Mohawk"
	icon_state = "teshari_fluffymohawk"
// MARKINGS

/datum/sprite_accessory/marking/teshari
	icon = 'icons/mob/human_races/species/teshari/marking.dmi'
	species_allowed = list(SPECIES_TESHARI)
	do_colouration = 1
